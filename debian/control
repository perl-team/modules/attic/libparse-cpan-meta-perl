Source: libparse-cpan-meta-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Vincent Danjean <vdanjean@debian.org>,
           Brian Cassidy <brian.cassidy@gmail.com>,
           Jonathan Yu <jawnsy@cpan.org>,
           gregor herrmann <gregoa@debian.org>,
           Ryan Niebur <ryan@debian.org>,
           Damyan Ivanov <dmn@debian.org>
Section: perl
Priority: optional
Build-Depends: debhelper (>= 8)
Build-Depends-Indep: perl (>= 5.19.9) | libcpan-meta-yaml-perl (>= 0.011),
                     perl,
                     perl (>= 5.15.0) | libjson-pp-perl
Standards-Version: 3.9.5
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/libparse-cpan-meta-perl.git
Vcs-Git: git://anonscm.debian.org/pkg-perl/packages/libparse-cpan-meta-perl.git
Homepage: https://metacpan.org/release/Parse-CPAN-Meta

Package: libparse-cpan-meta-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         perl (>= 5.19.9) | libcpan-meta-yaml-perl (>= 0.011),
         perl (>= 5.15.0) | libjson-pp-perl
Description: module to parse META.yml and other similar CPAN metadata files
 Parse::CPAN::Meta is a parser for META.yml files, based on the parser from
 YAML::Tiny. Since the Tiny implementation supports a basic subset of the
 full YAML specification, it has enough parts to implement parsing of typical
 META.yml files. Parse::CPAN::Meta provides a very simply API of only two
 functions, based on the YAML functions of the same name. Wherever possible,
 identical calling semantics are used.
