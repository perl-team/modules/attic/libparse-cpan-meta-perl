libparse-cpan-meta-perl (1.4414-2) UNRELEASED; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 16 Aug 2014 09:34:09 +0200

libparse-cpan-meta-perl (1.4414-1) unstable; urgency=medium

  * New upstream release.
  * Strip trailing slash from metacpan URLs.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 Apr 2014 19:04:52 +0200

libparse-cpan-meta-perl (1.4413-1) unstable; urgency=medium

  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Install CONTRIBUTING file.
  * New upstream release.
  * Update list of copyright holders and copyright years.
  * Update build dependencies.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 23 Feb 2014 18:48:32 +0100

libparse-cpan-meta-perl (1.4409-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Oct 2013 16:48:56 +0200

libparse-cpan-meta-perl (1.4407-1) unstable; urgency=low

  * New upstream release.
  * debian/rules: remove override for tests. AUTOMATED_TESTING doesn't
    exist anymore on the test suite.
  * Add new upstream copyright holder.
  * Switch order of alternative (build) dependencies now that Perl 5.18 is
    uploaded.
  * Drop build dependencies that are not needed anymore.

 -- gregor herrmann <gregoa@debian.org>  Tue, 17 Sep 2013 19:14:04 +0200

libparse-cpan-meta-perl (1.4405-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Update years of upstream and packaging copyright.
  * Set Standards-Version to 3.9.4 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Wed, 24 Jul 2013 20:17:58 +0200

libparse-cpan-meta-perl (1.4404-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Mon, 09 Apr 2012 19:22:59 +0200

libparse-cpan-meta-perl (1.4403-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Update (build) depends on libparse-cpan-meta-perl and libjson-pp-perl
  * Update debian/copyright format as in Debian Policy 3.9.3
  * Bump Standards-Version to 3.9.3

 -- Alessandro Ghedini <ghedo@debian.org>  Sun, 01 Apr 2012 16:32:26 +0200

libparse-cpan-meta-perl (1.4402-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of packaging copyright.
  * debian/rules: set AUTOMATED_TESTING only for tests.
  * Switch to debhelper compatibility level 8.
  * Bump Standards-Version to 3.9.2 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Feb 2012 23:51:18 +0100

libparse-cpan-meta-perl (1.4401-1) unstable; urgency=low

  [ Ryan Niebur ]
  * Email change: Ryan Niebur -> ryan@debian.org
  * Email change: Jonathan Yu -> jawnsy@cpan.org

  [ gregor herrmann ]
  * Add perl (>= 5.10.1) as an alternative build dependency for
    Pod::Simple.

  [ Nicholas Bamber ]
  * Added myself to Uploaders
  * Refreshed copyright using experimental dh-make-perl
  * New upstream release
  * Added debian/source/format file
  * Added libjson-pp-perl as a dependency
  * Decapitalized short description

  [ Jonathan Yu ]
  * New upstream release

  [ Ansgar Burchardt ]
  * Use tiny debian/rules.

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Fri, 11 Mar 2011 09:08:10 +0000

libparse-cpan-meta-perl (1.40-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Closes: #542676 -- newer upstream release bundled with Perl 5.10.1
  * Added myself to Uploaders and Copyright
  * Cleaned up copyright and dependencies
  * Standards-Version 3.8.2 (no changes)

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Damyan Ivanov ]
  * Standards-Version: 3.8.3
    + drop the version from perl build-dependency (satisfied by oldstable)
  * add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Fri, 21 Aug 2009 11:39:28 +0300

libparse-cpan-meta-perl (1.38-1) unstable; urgency=low

  * New upstream release
  * Add myself to Uploaders
  * add build deps needed for the tests

 -- Ryan Niebur <ryanryan52@gmail.com>  Sun, 17 May 2009 13:44:11 -0700

libparse-cpan-meta-perl (0.05-1) unstable; urgency=low

  * New upstream release.
  * Add /me to Uploaders.
  * Set Standards-Version to 3.8.1 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Thu, 12 Mar 2009 23:33:26 +0100

libparse-cpan-meta-perl (0.04-1) unstable; urgency=low

  [ Brian Cassidy ]
  * New upstream release
  * debian/control:
  + added myself to Uploaders
  + changed maintainer to pkg-perl group
  * debian/copyright: update to new format

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza).

 -- Brian Cassidy <brian.cassidy@gmail.com>  Thu, 15 Jan 2009 20:17:52 -0400

libparse-cpan-meta-perl (0.03-1) unstable; urgency=low

  * debian/control:
    build-depends on "lib...-perl | perl" in order to break circular
    build-dependencies loops for autobuilder (this just skips some checks)
  * Initial release. (Closes: #495205: ITP: libparse-cpan-meta-perl --
    module to parse META.yml and other similar CPAN metadata files)

 -- Vincent Danjean <vdanjean@debian.org>  Fri, 15 Aug 2008 12:48:21 +0200
